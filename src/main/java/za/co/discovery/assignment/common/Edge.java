/**
 * 
 */
package za.co.discovery.assignment.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Edge {

	@XmlElement
	private String id;
	@XmlElement
	private Vertex origin;
	@XmlElement
	private Vertex destination;
	@XmlElement
	private double distance;
	@XmlElement
	private double traffic;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the origin
	 */
	public Vertex getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(Vertex origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public Vertex getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(Vertex destination) {
		this.destination = destination;
	}

	/**
	 * @return the distance
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}

	/**
	 * @return the traffic
	 */
	public double getTraffic() {
		return traffic;
	}

	/**
	 * @param traffic the traffic to set
	 */
	public void setTraffic(double traffic) {
		this.traffic = traffic;
	}

}
