/**
 * 
 */
package za.co.discovery.assignment.common;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Nhlanhla
 *
 * 04 Feb 2020
 */

@XmlRootElement(name = "Vertex")
@XmlAccessorType(XmlAccessType.FIELD)
public class Vertex  implements Serializable{
	
	private static final long serialVersionUID = -4533107123005100193L;
	
	
	@XmlElement
	private String id;
	@XmlElement
	private String planet;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the planet
	 */
	public String getPlanet() {
		return planet;
	}
	/**
	 * @param planet the planet to set
	 */
	public void setPlanet(String planet) {
		this.planet = planet;
	}
}

