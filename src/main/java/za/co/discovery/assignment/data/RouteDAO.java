/**
 * 
 */
package za.co.discovery.assignment.data;

import javax.enterprise.context.RequestScoped;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@RequestScoped
public class RouteDAO {

	Connection conn = null;
	Statement stmt = null;
	String connUrl = "jdbc:derby:memory:pathDB;create=true";
	String driver = "org.apache.derby.jdbc.EmbeddedDriver";

	public ResultSet getPlanteDestinationInfo(String id)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName(driver).newInstance();
		conn = DriverManager.getConnection(connUrl);
		stmt = conn.createStatement();

		if (id.indexOf("'") > -1)
			id = "'" + id.substring(0, id.indexOf("'")) + "'" + id.substring(id.indexOf("'"), id.length()) + "'";
		else
			id = "'" + id + "'";

		ResultSet rs = stmt.executeQuery("SELECT * FROM ROUTES WHERE PLANET_ORIGIN = " + id);

		return rs;
	}

	public ResultSet getVertex()
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Class.forName(driver).newInstance();
		conn = DriverManager.getConnection(connUrl);
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM PLANETS");
		return rs;
	}
}
