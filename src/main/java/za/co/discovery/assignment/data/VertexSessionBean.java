/**
 * 
 */
package za.co.discovery.assignment.data;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;

import za.co.discovery.assignment.common.Vertex;

/**
 * @author Nhlanhla
 *
 * 04 Feb 2020
 */


@SessionScoped
@ManagedBean
public class VertexSessionBean implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -415054217001554033L;
	
	
	private List<Vertex> nodes;

	/**
	 * @return the nodes
	 */
	public List<Vertex> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes the nodes to set
	 */
	public void setNodes(List<Vertex> nodes) {
		this.nodes = nodes;
	}
	
}
