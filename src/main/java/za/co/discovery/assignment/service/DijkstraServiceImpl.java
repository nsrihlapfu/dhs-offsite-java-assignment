/**
 * 
 */
package za.co.discovery.assignment.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import za.co.discovery.assignment.common.Edge;
import za.co.discovery.assignment.common.Vertex;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

public class DijkstraServiceImpl implements DijkstraService {

	@PostConstruct
	void init() {
		// shortestPathService should not be null here.
	}

	ShortestPathService shortestPathService = new ShortestPathServiceImpl();

	private Set<Vertex> visitedVertexes;
	private Set<Vertex> unVisitedVertexes;
	private Map<Vertex, Vertex> previousVertex;
	private Map<Vertex, Double> distance;
	private Map<Vertex, Double> traffic;

	public void process(Vertex source, boolean withTraffic) {
		visitedVertexes = new HashSet<Vertex>();
		unVisitedVertexes = new HashSet<Vertex>();
		distance = new HashMap<Vertex, Double>();
		traffic = new HashMap<Vertex, Double>();
		previousVertex = new HashMap<Vertex, Vertex>();
		distance.put(source, 0d);

		unVisitedVertexes.add(source);

		while (unVisitedVertexes.size() > 0) {
			Vertex vertex = getMin(unVisitedVertexes);
			visitedVertexes.add(vertex);
			unVisitedVertexes.remove(vertex);
			getMinimumDistance(vertex);
		}
	}

	public void getMinimumDistance(Vertex node) {
		List<Edge> adjacentConnections = shortestPathService.getAdjacentNode(node.getId());
		List<Vertex> adjacentVertexes = getAdjacentVertexes(adjacentConnections, node);
		for (Vertex vertex : adjacentVertexes) {
			if (getShortestDistance(vertex) > getShortestDistance(node)
					+ getDistance(node, vertex, adjacentConnections)) {
				distance.put(vertex, getShortestDistance(node) + getDistance(node, vertex, adjacentConnections));
				previousVertex.put(vertex, node);
				unVisitedVertexes.add(vertex);
			}
		}
	}

	// Get adjacent nodes from list of edges
	private List<Vertex> getAdjacentVertexes(List<Edge> edges, Vertex node) {
		List<Vertex> vertexes = new ArrayList<Vertex>();
		for (Edge edge : edges) {
			if (edge.getOrigin().getId().equals(node.getId()) && !isVisited(edge.getDestination())) {
				vertexes.add(edge.getDestination());
			}
		}
		return vertexes;
	}

	// Check if vertex/node is visited
	private boolean isVisited(Vertex vertex) {
		return visitedVertexes.contains(vertex);
	}

	private Vertex getMin(Set<Vertex> vertexes) {
		Vertex min = null;
		for (Vertex vertex : vertexes) {
			if (null == min) {
				min = vertex;
			} else {
				if (getShortestDistance(vertex) < getShortestDistance(min)) {
					min = vertex;
				}
			}
		}
		return min;
	}

	private double getDistance(Vertex node, Vertex target, List<Edge> edges) {
		for (Edge edge : edges) {
			if (edge.getOrigin().getId().equals(node.getId()) && edge.getDestination().getId().equals(target.getId())) {
				return edge.getDistance();
			}
		}
		throw new RuntimeException("Runtime Exception");
	}

	// Get shortest distance or set to max if distance is null
	private double getShortestDistance(Vertex destination) {
		Double d = distance.get(destination);
		if (d == null) {
			return Double.MAX_VALUE;
		} else {
			return d;
		}
	}

	// Get shortest traffic or set to max if traffic not found
	private double getShortestTrafficDelay(Vertex destination) {
		Double d = traffic.get(destination);
		if (d == null) {
			return Double.MAX_VALUE;
		} else {
			return d;
		}
	}

	@Override
	public LinkedList<Vertex> getShortestPath(Vertex destination) {
		LinkedList<Vertex> shortestPath = new LinkedList<Vertex>();
		Vertex path = destination;

		shortestPath.add(path);

		// previousVertex.get does not work using the param key : path/destination.
		for (Map.Entry<Vertex, Vertex> enrty : previousVertex.entrySet()) {
			if (enrty.getKey().getId().equals(path.getId())) {
				path = enrty.getKey();
				break;
			}
		}

		while (null != previousVertex.get(path)) {
			System.out.println("Size : " + previousVertex.get(path).hashCode());
			path = previousVertex.get(path);
			shortestPath.add(path);
		}

		Collections.reverse(shortestPath);
		shortestPath.removeFirst();
		return shortestPath;
	}

	@Override
	public LinkedList<Vertex> getShortestTraffic(Vertex destination) {
		// TODO Auto-generated method stub
		return null;
	}
}
