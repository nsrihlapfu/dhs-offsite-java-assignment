/**
 * 
 */
package za.co.discovery.assignment.service;

import java.util.LinkedList;

import za.co.discovery.assignment.common.Vertex;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

public interface DijkstraService {

	public LinkedList<Vertex> getShortestPath(Vertex destination);

	public LinkedList<Vertex> getShortestTraffic(Vertex destination);
}
