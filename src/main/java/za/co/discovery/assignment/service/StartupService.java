/**
 * 
 */
package za.co.discovery.assignment.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import za.co.discovery.assignment.client.DijkstraClient;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@Startup
@Singleton
public class StartupService {
	private static final Logger LOGGER = Logger.getLogger(StartupService.class.getName());

	@Inject
	DijkstraClient dijstraClient;

	private static Connection conn = null;
	private static Statement stmt = null;
	private static String connUrl = "jdbc:derby:memory:pathDB;create=true";
	public String driver = "org.apache.derby.jdbc.EmbeddedDriver";

	@PostConstruct
	private void init() {
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(connUrl);
			stmt = conn.createStatement();
			stmt.executeUpdate("CREATE TABLE PLANETS (NODE VARCHAR(15) PRIMARY KEY, NAME VARCHAR(55))");
			stmt.executeUpdate(
					"CREATE TABLE ROUTES (ID INT PRIMARY KEY, PLANET_ORIGIN VARCHAR(15), PLANET_DESTINATION VARCHAR(15), DISTANCE DOUBLE, TRAFFIC DOUBLE, FOREIGN KEY(PLANET_ORIGIN) REFERENCES PLANETS(NODE), FOREIGN KEY(PLANET_DESTINATION) REFERENCES PLANETS(NODE))");
			doPlanetsBulkUpload();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void doPlanetsBulkUpload() {
		final String FILE_NAME = "/files/interstellar.xlsx";
		Workbook workbook = null;
		try {
			workbook = new XSSFWorkbook(this.getClass().getResourceAsStream(FILE_NAME));
			uploadPlanets(workbook.getSheetAt(0));
			uploadRouteAndTraffic(workbook.getSheetAt(1));
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		} finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e2) {
					LOGGER.log(Level.WARNING, "Error closing Workbook from file '" + FILE_NAME + "'.", e2);
				}
			}
		}
	}

	private void uploadPlanets(Sheet sheet) {
		int rowIndex = 0;
		Iterator<Row> rowIterator = sheet.iterator();
		String planetNode = null, planetName = null;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (rowIndex >= 1) {
				int columnIndex = 0;
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell column = cellIterator.next();
					switch (columnIndex) {
					case 0:
						planetNode = column.getStringCellValue().trim();
						break;
					case 1:
						planetName = column.getStringCellValue().trim();
						break;
					default:
						break;
					}
					columnIndex++;
				}
				try {
					// TODO : CHAR function can be an alternative
					// Handle Apostrophe for
					if (planetNode.indexOf("'") > -1)
						planetNode = "'" + planetNode.substring(0, planetNode.indexOf("'")) + "'"
								+ planetNode.substring(planetNode.indexOf("'"), planetNode.length()) + "'";
					else
						planetNode = "'" + planetNode + "'";

					if (planetName.indexOf("'") > -1)
						planetName = "'" + planetName.substring(0, planetName.indexOf("'")) + "'"
								+ planetName.substring(planetName.indexOf("'"), planetName.length()) + "'";
					else
						planetName = "'" + planetName + "'";

					stmt.executeUpdate("INSERT INTO PLANETS VALUES(" + planetNode + "," + planetName + ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			rowIndex++;
		}
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM PLANETS");
			while (rs.next()) {
				System.out.printf("%s\t%s\n", rs.getString("NODE"), rs.getString("NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void uploadRouteAndTraffic(Sheet sheet) {
		int rowIndex = 0;
		Iterator<Row> rowIterator = sheet.iterator();
		String planetOrigin = null, planetDestination = null;
		double distance = 0.0;
		double traffic = 0.0;
		int id = 0;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (rowIndex >= 1) {
				int columnIndex = 0;
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell column = cellIterator.next();
					switch (columnIndex) {
					case 0:
						id = (int) column.getNumericCellValue();
						break;
					case 1:
						planetOrigin = column.getStringCellValue().trim();
						break;
					case 2:
						planetDestination = column.getStringCellValue().trim();
						break;
					case 3:
						distance = column.getNumericCellValue();
						break;
					case 4:
						traffic = column.getNumericCellValue();
						break;
					default:
						break;
					}
					columnIndex++;
				}
				try {
					// TODO : CHAR function can be an alternative
					// Handle Apostrophe for
					if (planetOrigin.indexOf("'") > -1)
						planetOrigin = "'" + planetOrigin.substring(0, planetOrigin.indexOf("'")) + "'"
								+ planetOrigin.substring(planetOrigin.indexOf("'"), planetOrigin.length()) + "'";
					else
						planetOrigin = "'" + planetOrigin + "'";

					if (planetDestination.indexOf("'") > -1)
						planetDestination = "'" + planetDestination.substring(0, planetDestination.indexOf("'")) + "'"
								+ planetDestination.substring(planetDestination.indexOf("'"),
										planetDestination.length())
								+ "'";
					else
						planetDestination = "'" + planetDestination + "'";

					stmt.executeUpdate("INSERT INTO ROUTES VALUES(" + id + "," + planetOrigin + "," + planetDestination
							+ "," + distance + "," + traffic + ")");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			rowIndex++;
		}

		// Captured routes
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM ROUTES");
			while (rs.next()) {
				System.out.printf("%d\t%s\t%s\t%d\t%d\n", rs.getInt("ID"), rs.getString("PLANET_ORIGIN"),
						rs.getString("PLANET_DESTINATION"), rs.getLong("DISTANCE"), rs.getLong("TRAFFIC"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
