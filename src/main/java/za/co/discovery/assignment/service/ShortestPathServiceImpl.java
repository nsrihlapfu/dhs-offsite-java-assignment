/**
 * 
 */
package za.co.discovery.assignment.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import za.co.discovery.assignment.common.Edge;
import za.co.discovery.assignment.common.Vertex;
import za.co.discovery.assignment.data.RouteDAO;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@Stateless
public class ShortestPathServiceImpl implements ShortestPathService {

	
	RouteDAO routeDAO = new RouteDAO();

	@Override
	public List<Edge> getAdjacentNode(String id) {
		List<Edge> edges = new ArrayList<Edge>();
		try {
			ResultSet rs = routeDAO.getPlanteDestinationInfo(id);
			while (rs.next()) {
				Vertex origin = new Vertex();
				origin.setId(rs.getString("PLANET_ORIGIN"));
				origin.setPlanet(rs.getString("PLANET_ORIGIN"));
				Vertex destination = new Vertex();
				destination.setId(rs.getString("PLANET_DESTINATION"));
				destination.setPlanet(rs.getString("PLANET_DESTINATION"));
				Edge edge = new Edge();
				edge.setOrigin(origin);
				edge.setDestination(destination);
				edge.setDistance(rs.getDouble("DISTANCE"));
				edge.setTraffic(rs.getDouble("TRAFFIC"));

				edges.add(edge);
			}
			return edges;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Vertex> getVertex() {
		List<Vertex> nodes = new ArrayList<Vertex>();
		try {
			ResultSet rs;
			rs = routeDAO.getVertex();
			while (rs.next()) {
				Vertex node = new Vertex();
				node.setId(rs.getString("NODE"));
				node.setPlanet(rs.getString("NAME"));
				nodes.add(node);
			}
			return nodes;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("An SQL Exception occured");
		}
	}
}
