/**
 * 
 */
package za.co.discovery.assignment.service;

import java.util.List;

import za.co.discovery.assignment.common.Edge;
import za.co.discovery.assignment.common.Vertex;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

public interface ShortestPathService {

	public List<Edge> getAdjacentNode(final String id);

	public List<Vertex> getVertex();
}
