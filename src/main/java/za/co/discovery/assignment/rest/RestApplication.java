/**
 * 
 */
package za.co.discovery.assignment.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Nhlanhla
 *
 * 04 Feb 2020
 */

@ApplicationPath("/rest")
public class RestApplication extends Application{

}
