/**
 * 
 */
package za.co.discovery.assignment.resource;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import za.co.discovery.assignment.common.Edge;
import za.co.discovery.assignment.common.Vertex;
import za.co.discovery.assignment.service.DijkstraServiceImpl;
import za.co.discovery.assignment.service.ShortestPathService;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@Path("/dijstra")
@Produces("application/xml")
public class AssignmentResource {

	@Inject
	private ShortestPathService shortestPathService;

	@GET
	@Path("/vertex/{id}")
	public List<Edge> getVertexById(@PathParam("id") String vertexId) {
		return shortestPathService.getAdjacentNode(vertexId);
	}

	@GET
	@Path("/vertex")
	public List<Vertex> getVertex() {
		return shortestPathService.getVertex();
	}

	@GET
	@Path("/vertex/{source}/{destination}/{traffic}")
	public LinkedList<Vertex> getPaths(@PathParam("source") String source, @PathParam("destination") String destination,
			@PathParam("traffic") boolean traffic) {
		DijkstraServiceImpl dijstraServiceImpl = new DijkstraServiceImpl();
		Vertex vertex = new Vertex();
		vertex.setId(source);
		vertex.setPlanet(source);
		dijstraServiceImpl.process(vertex, traffic);
		vertex.setId(destination);
		vertex.setPlanet(destination);
		return dijstraServiceImpl.getShortestPath(vertex);
	}
}
