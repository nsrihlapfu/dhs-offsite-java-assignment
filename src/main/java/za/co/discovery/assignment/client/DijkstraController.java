/**
 * 
 */
package za.co.discovery.assignment.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import za.co.discovery.assignment.common.Vertex;
import za.co.discovery.assignment.data.VertexSessionBean;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@Named
@ViewScoped
public class DijkstraController implements Serializable {

	private static final long serialVersionUID = -3816805371933691298L;

	@EJB
	private VertexSessionBean vertexSessionBean;

	@Inject
	private DijkstraClient dijstraClient;

	private List<Vertex> nodes;
	private List<Vertex> shortestDistance;
	private List<Vertex> shortestTraffic;
	private List<String> planets = new ArrayList<String>();
	private String destinationVertex;

	@PostConstruct
	private void init() {
		findNodes();
	}

	private void findNodes() {
		if (planets == null || planets.isEmpty()) {
			nodes = dijstraClient.getVertex();
			for (Vertex n : nodes) {
				planets.add(n.getPlanet());
			}
			planets.remove("Earth");
		}
	}

	public void findShortestDistance() {
		String destination = null;
		if (null != nodes) {
			for (Vertex n : nodes) {
				if (n.getPlanet().equals(destinationVertex)) {
					destination = n.getId();
				}
			}
		} else {
			findNodes();
			findShortestDistance();
		}
		if (null == destination) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage("Destination is not set, please a destination");
			message.setSeverity(FacesMessage.SEVERITY_WARN);
			context.addMessage(null, message);
		}
		shortestDistance = dijstraClient.getShortestPath("A", destination, false);
		
	}

	public void findShortestTraffic(String originVertex) {
	}

	
	
	

	/**
	 * @return the destinationVertex
	 */
	public String getDestinationVertex() {
		return destinationVertex;
	}

	/**
	 * @param destinationVertex the destinationVertex to set
	 */
	public void setDestinationVertex(String destinationVertex) {
		this.destinationVertex = destinationVertex;
	}

	/**
	 * @return the planets
	 */
	public List<String> getPlanets() {
		return planets;
	}

	/**
	 * @param planets the planets to set
	 */
	public void setPlanets(List<String> planets) {
		this.planets = planets;
	}

	/**
	 * @return the vertexSessionBean
	 */
	public VertexSessionBean getVertexSessionBean() {
		return vertexSessionBean;
	}

	/**
	 * @param vertexSessionBean the vertexSessionBean to set
	 */
	public void setVertexSessionBean(VertexSessionBean vertexSessionBean) {
		this.vertexSessionBean = vertexSessionBean;
	}

	/**
	 * @return the shortestDistance
	 */
	public List<Vertex> getShortestDistance() {
		return shortestDistance;
	}

	/**
	 * @param shortestDistance the shortestDistance to set
	 */
	public void setShortestDistance(List<Vertex> shortestDistance) {
		this.shortestDistance = shortestDistance;
	}

	/**
	 * @return the shortestTraffic
	 */
	public List<Vertex> getShortestTraffic() {
		return shortestTraffic;
	}

	/**
	 * @param shortestTraffic the shortestTraffic to set
	 */
	public void setShortestTraffic(List<Vertex> shortestTraffic) {
		this.shortestTraffic = shortestTraffic;
	}

	/**
	 * @return the nodes
	 */
	public List<Vertex> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes the nodes to set
	 */
	public void setNodes(List<Vertex> nodes) {
		this.nodes = nodes;
	}
}
