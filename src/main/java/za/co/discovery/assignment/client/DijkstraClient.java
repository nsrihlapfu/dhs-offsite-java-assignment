/**
 * 
 */
package za.co.discovery.assignment.client;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import za.co.discovery.assignment.common.Vertex;

/**
 * @author Nhlanhla
 *
 *         04 Feb 2020
 */

@ViewScoped
public class DijkstraClient implements Serializable {

	
	private static final long serialVersionUID = 3381116359082886754L;

	public List<Vertex> getVertex() {

		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://localhost:8080/nhlanhlaRihlapfu/rest/dijstra/vertex");
			ClientResponse response = webResource.accept("application/*").get(ClientResponse.class);

			List<Vertex> nodes = response.getEntity(new GenericType<List<Vertex>>() {
			});
			return nodes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<Vertex> getShortestPath(String source, String destination, boolean withTraffic) {
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://localhost:8080/nhlanhlaRihlapfu/rest/dijstra/vertex/" + source
					+ "/" + destination + "/" + withTraffic);
			ClientResponse response = webResource.accept("application/*").get(ClientResponse.class);

			List<Vertex> paths = (List<Vertex>) response.getEntity(new GenericType<List<Vertex>>() {
			});
			return paths;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
