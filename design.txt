Submission notes/assumptions

1. The following is done:
	- Persist graph into an in-memory database.
		* Derby
	- Read file and import it into the database.
	- Expose database using RESTful webservice.
	- Implemented the algorithm to describe the routes
		* Add source(A Earth) to unvisited nodes.
		* Set its distance to zero.
		* While unvisited nodes is not empty, get the minimal distance from the nodes.
		* Remove source from unvisited and add it to the visited nodes.
		* Evaluate the source's adjacent nodes and add them to unvisited nodes.
		* From the adjacent nodes get the one with minimum distance and make it the new source.
		* One with minimum distance becomes the source.
	Repeated the process until there is no visited node remaining.
	
	- Exposed the algorithm using webservice
		* Requests and responses.
	- Create front-end to capture source and destination.


2. Assumptions for future improvement
	- Display the shortest distance by names with respective distance/traffic.
	- Make the UI more attractive.
	- Keep data in session.
	- All the above suggestion are doable by adding one or two methods without having to change current code. Code is reusable.
	
	
3. Improvements on assignment.
	- Ensure that Planet L' exists on the Planet Tab, I have added one and named it Discovery. This took some time to figure out.
	- Row 43 on both routes and traffic tabs, almost made me think that my algorithm wasn't working. Lot of  time was spent trying to figure this out.
	
	
Developed using RedHat Code Ready Studio
Tested on Jboss EAP
This is a Maven project